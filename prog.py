import math
from timeit import default_timer as timer


def f1(n):
    s = 0
    for j in range(1, n):
        s = s + 1 / j
    return s


def f2(n):
    s = 0
    for j in range(1, n):
        for k in range(1, n):
            s = s + k / j
    return s


def f3(n):
    s = 0
    for j in range(1, n):
        for k in range(j, n):
            s = s + k / j
    return s


def f4(n):
    s = 0
    for j in range(1, n):
        k = 2
        while k <= n:
            s = s + k / j
            k = k * 2
    return s


def f5(n):
    s = 0
    k = 2
    while k <= n:
        s = s + 1 / k
        k = k * 2
    return s


nn = [2000, 4000, 8000, 16000, 32000]

#funkcja nr 1
print("Funkcja nr1:")
for n in nn:
    start = timer()
    f1(n)
    stop = timer()
    Tn = stop - start
    Fn = n
    print(n, Tn, Fn / Tn)

#funkcja nr 2
print("Funkcja nr2:")
for n in nn:
    start = timer()
    f2(n)
    stop = timer()
    Tn = stop - start
    Fn = n*n
    print(n, Tn, Fn / Tn)

#funkcja nr 3
print("Funkcja nr3:")
for n in nn:
    start = timer()
    f3(n)
    stop = timer()
    Tn = stop - start
    Fn = n*n
    print(n, Tn, Fn / Tn)

print("Funkcja nr4:")
for n in nn:
    start = timer()
    f4(n)
    stop = timer()
    Tn = stop - start
    Fn = n*math.log(n,2)
    print(n, Tn, Fn / Tn)

print("Funkcja nr5:")
for n in nn:
    start = timer()
    f5(n)
    stop = timer()
    Tn = stop - start
    Fn = math.log(n, 2)
    print(n, Tn, Fn / Tn)

#wygenerowane pomiary:
# Funkcja nr1:
# 2000 0.00011139999999999761 17953321.36445281
# 4000 0.00019500000000000073 20512820.512820438
# 8000 0.00039909999999999946 20045101.47832626
# 16000 0.0008173999999999994 19574259.8482995
# 32000 0.0016283999999999986 19651191.35347582
# Funkcja nr2:
# 2000 0.19427680000000003 20589179.97413999
# 4000 0.7874024000000001 20319978.704662316
# 8000 3.1198233 20513982.31431889
# 16000 12.468986599999997 20530938.737234674
# 32000 49.5773028 20654612.941146124
# Funkcja nr3:
# 2000 0.09448030000000074 42336868.10901287
# 4000 0.37858540000000573 42262591.21455756
# 8000 1.5087092999999925 42420365.54026698
# 16000 6.125854099999998 41790090.95237839
# 32000 24.18358640000001 42342768.48201471
# Funkcja nr4:
# 2000 0.0015608000000071343 14051491.907498673
# 4000 0.003455399999992892 13851692.174204668
# 8000 0.007458399999990206 13907309.111529673
# 16000 0.01616520000000321 13823061.177996501
# 32000 0.03488100000001282 13729683.699120175
# Funkcja nr5:
# 2000 1.499999996212864e-06 7310522.874898687
# 4000 1.3000000080864993e-06 9204449.392484857
# 8000 1.1999999998124622e-06 10804820.238907
# 16000 1.2999999938756446e-06 10742911.038811916
# 32000 1.3000000080864993e-06 11512141.685822431


